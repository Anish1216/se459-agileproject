# SE459-AgileProject
Final Project for SE459

## Description ##
This project simulates an automated vacuum called the CleanSweep, which traverses a house, vacuuming dirt. When it runs out of charge, it returns to the nearest charging station. When its bag is full, it returns to its base and powers off, indicating that it needs to be emptied.

We employed agile software development practices for this project. We developed the application in three two-week iterations, with regular meetings and retrospectives at the end of each iteration. Some of the specific techniques we used involve:

* Pair programming
* Unit testing
* Continuous integration (Jenkins)
* Automated builds (Maven)

### Authors###
- Alex Dantoft
- Adnan Dossaji
- Anish Krishnan
- Charles Marcille
